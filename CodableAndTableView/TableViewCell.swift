//
//  TableViewCell.swift
//  CodableAndTableView
//
//  Created by Akshat Jain on 12/10/20.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var userID: UILabel!
    
    @IBOutlet weak var userTitle: UILabel!
    
    @IBOutlet weak var userCompleted: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
