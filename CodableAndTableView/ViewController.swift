//
//  ViewController.swift
//  CodableAndTableView
//
//  Created by Akshat Jain on 12/10/20.
//

import UIKit

struct jsonStruct: Decodable {
    let id: Int
    let title: String
    let completed:Bool
    
}

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableview: UITableView!
    
    var arrData = [jsonStruct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getData()
    }
    
    func getData(){
        let serviceUrl = URL(string: "https://jsonplaceholder.typicode.com/todos/")
        URLSession.shared.dataTask(with: serviceUrl!) { (data, response, error) in
            
            do{
                if error == nil{
                    self.arrData = try JSONDecoder().decode([jsonStruct].self, from: data!)
                    for mainarr in self.arrData{
                        print(mainarr.id)
                        DispatchQueue.main.async {
                            self.tableview.reloadData()
                            
                        }
                    }
                }
            }catch{
                print("Error in get data in json")
            }
        }.resume()
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TableViewCell = tableview.dequeueReusableCell(withIdentifier: "cell") as! TableViewCell
        
        cell.userID.text = "user ID : \(arrData[indexPath.row].id)"
        cell.userTitle.text = "user text : \(arrData[indexPath.row].title)"
        cell.userCompleted.text = "user Completion : \(arrData[indexPath.row].completed)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}
